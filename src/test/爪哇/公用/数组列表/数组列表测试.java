package test.爪哇.公用.数组列表;

import static org.junit.Assert.assertTrue;

import java.util.Collections;

import main.爪哇.公用.数组列表;

import org.junit.Test;

public class 数组列表测试 {

  // TODO: 添加测试“取”方法
  
  @Test
  public void 测试添加元素类() {
    数组列表<Integer> 表 = new 数组列表<>();
    表.添加(1);
    assertTrue(表.长度() == 1);
  }

  @Test
  public void 测试添加整数元素类() {
    数组列表<Integer> 表 = new 数组列表<>();
    表.添加(0, 2);
    assertTrue(表.长度() == 1);
    表.添加(0, 1);
    assertTrue(表.长度() == 2);
  }

  @Test
  public void 测试Collections方法() {
    数组列表<Integer> 表 = new 数组列表<>();
    表.添加(2);
    表.添加(1);
    Collections.sort(表.原型());
    assertTrue(表.取(0) == 1);
  }
}
