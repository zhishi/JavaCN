package test.工具.解析.java;

import static org.junit.Assert.assertTrue;

import java.io.File;

import main.工具.公用.FileUtil;
import main.工具.解析.java.类型名提取器;
import main.工具.解析.java.类型名提取器.类型名;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.junit.Test;

public class 类型名提取器Test {

  private static final ASTParser 语法解析器 = ASTParser.newParser(AST.JLS8);
  private static final 类型名提取器 提取器 = new 类型名提取器();

  @Test
  public void test获取类结构() {
    try {
      语法解析器.setSource(FileUtil.getStringFromSourceFile(new File("jdkSrc/ArrayList.java"))
          .toCharArray());
    } catch (Exception e) {
      assertTrue(false);
    }
    语法解析器.createAST(null).accept(提取器);
    类型名 名 = 提取器.获取名();
    assertTrue(名.类名.size() > 0);
    assertTrue(名.方法名.size() > 0);
    assertTrue(名.参数名.size() > 0);
  }

}
