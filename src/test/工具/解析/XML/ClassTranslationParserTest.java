package test.工具.解析.XML;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import main.工具.解析.XML.ClassTranslation;
import main.工具.解析.XML.ClassTranslationParser;
import main.工具.解析.XML.Translation;

import org.junit.Test;

public class ClassTranslationParserTest {

  @Test
  public void testParse() {
    List<ClassTranslation> classes;
    try {
      classes = ClassTranslationParser.parse("translationArrayList.xml");
      assertEquals(1, classes.size());
      ClassTranslation classTranslation = classes.get(0);
      assertEquals("ArrayList", classTranslation.getClassName().getEn());
      assertEquals("数组列表", classTranslation.getClassName().getCn());
      
      List<Translation> packages = classTranslation.getPackages();
      assertEquals(2, packages.size());
      assertEquals("java", packages.get(0).getEn());
      assertEquals("爪哇", packages.get(0).getCn());
      assertEquals("util", packages.get(1).getEn());
      assertEquals("工具", packages.get(1).getCn());
      
      Map<String, String> methods = classTranslation.getMethods();
      assertEquals(2, methods.size());
      assertEquals("添加", methods.get("add"));
      assertEquals("大小", methods.get("size"));
    } catch (FileNotFoundException | XMLStreamException e) {
      assertTrue(false);
      e.printStackTrace();
    }
  }
}
