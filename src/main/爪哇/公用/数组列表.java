package main.爪哇.公用;

import java.util.ArrayList;
import java.util.Collection;

public class 数组列表<元素类> implements 列表<元素类> {
  private ArrayList<元素类> 对象;

  public 数组列表() {
    对象 = new ArrayList<元素类>();
  }

  // TODO: javadoc
  public 元素类 取(int 索引) {
    return 对象.get(索引);
  }

  /**
   * 将指定的元素添加到此列表的尾部。
   * 
   * @param 元素 要添加到此列表中的元素
   * @return <tt>true</tt> (按照 {@link Collection#add} 的指定)
   */
  public boolean 添加(元素类 元素) {
    return 对象.add(元素);
  }

  public void 添加(int 索引, 元素类 元素) {
    对象.add(索引, 元素);
  }

  public boolean 添加所有(Collection<? extends 元素类> 集合) {
    return 对象.addAll(集合);
  }

  public boolean 添加所有(int 索引, Collection<? extends 元素类> 集合) {
    return 对象.addAll(索引, 集合);
  }

  public void 确保容量(int 最小容量) {
    对象.ensureCapacity(最小容量);
  }

  public int 长度() {
    return 对象.size();
  }

  public <T> T[] 转为数组(T[] a) {
    return 对象.toArray(a);
  }

  public ArrayList<元素类> 原型() {
    return 对象;
  }
}
