package main.爪哇.公用;

import java.util.Collection;

public interface 列表<元素类> {
  
  boolean 添加(元素类 元素);

  boolean 添加所有(int 索引, Collection<? extends 元素类> 集合);

  int 长度();

  <T> T[] 转为数组(T[] a);
}
