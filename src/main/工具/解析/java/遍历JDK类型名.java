package main.工具.解析.java;

import java.io.File;

import main.工具.公用.FileUtil;
import main.工具.解析.java.类型名提取器.类型名;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;

public class 遍历JDK类型名 {

  private static final ASTParser 语法解析器 = ASTParser.newParser(AST.JLS8);

  // Config file path or directory path.
  private static final String FILE_PATH = "java/util/ArrayList.java";
  private static final String OUTPUT_DIR = "namelist/";

  private static final 类型名提取器 提取器 = new 类型名提取器();

  /**
   * 
   * @param args 第一个参数为JDK路径。可由JDK目录下的src.zip解压。
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    if (args.length != 1) {
      System.out.println("需要JDK源码路径作为唯一参数");
      return;
    }

    FileUtil.mkdir(OUTPUT_DIR);
    walkFile(new File(args[0] + FILE_PATH));

    类型名 名 = 提取器.获取名();
    // Remove constructor methods from method list
    名.方法名.removeAll(名.类名);

    FileUtil.writeLinesToFile(名.类名, OUTPUT_DIR + "classes.txt");
    FileUtil.writeLinesToFile(名.方法名, OUTPUT_DIR + "methods.txt");
    FileUtil.writeLinesToFile(名.参数名, OUTPUT_DIR + "parameters.txt");
    System.out.println("提取完毕: " + 名.类名.size() + "类;" + 名.方法名.size() + "方法;"
        + 名.参数名.size() + "参数");
  }

  private static void walkFile(File path) throws Exception {
    if (path.isFile()) {
      if (path.getName().endsWith(".java")) {
        parseFile(path);
      }
    } else {
      File[] files = path.listFiles();
      if (files != null) {
        for (File file : files) {
          walkFile(file);
        }
      }
    }
  }

  private static void parseFile(File file) throws Exception {
    语法解析器.setSource(FileUtil.getStringFromSourceFile(file).toCharArray());
    语法解析器.createAST(null).accept(提取器);
  }
}
