package main.工具.解析.java;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class 类结构提取器 extends ASTVisitor {

  private 类型结构 结构 = new 类型结构();

  @Override
  public boolean visit(MethodDeclaration node) {
    if (isPublicDeclaration(node)) {
      结构.方法.add(node);
    }
    return super.visit(node);
  }

  @Override
  public boolean visit(TypeDeclaration node) {
    if (isPublicDeclaration(node)) {
      结构.类型.add(node);
    }
    return super.visit(node);
  }

  public 类型结构 获取类结构() {
    return 结构;
  }

  private boolean isPublicDeclaration(BodyDeclaration node) {
    return (node.getModifiers() & Modifier.PUBLIC) != 0;
  }

  public class 类型结构 {
    public List<MethodDeclaration> 方法 = new ArrayList<>();
    public List<TypeDeclaration> 类型 = new ArrayList<>();
  }
}
