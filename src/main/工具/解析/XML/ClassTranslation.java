package main.工具.解析.XML;

import java.util.List;
import java.util.Map;

public class ClassTranslation {

  private List<Translation> packages;
  private Translation className;

  /**
   * 方法名翻译，en->cn
   */
  private Map<String, String> methods;

  public List<Translation> getPackages() {
    return packages;
  }

  public void setPackages(List<Translation> packages) {
    this.packages = packages;
  }

  public Translation getClassName() {
    return className;
  }

  public void setClassName(Translation className) {
    this.className = className;
  }

  public Map<String, String> getMethods() {
    return methods;
  }

  public void setMethods(Map<String, String> methods) {
    this.methods = methods;
  }
}
