package main.工具.解析.XML;

public class Translation {
  private String en;
  private String cn;

  public Translation(String en, String cn) {
    this.setEn(en);
    this.setCn(cn);
  }

  public String getCn() {
    return cn;
  }

  public void setCn(String cn) {
    this.cn = cn;
  }

  public String getEn() {
    return en;
  }

  public void setEn(String en) {
    this.en = en;
  }
}
