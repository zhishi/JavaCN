package main.工具.公用;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.function.Consumer;

public class FileUtil {
  public static String getStringFromSourceFile(String fileName) throws Exception {
    return getStringFromSourceFile(new File(fileName));
  }

  public static String getStringFromSourceFile(File file) throws Exception {
    StringBuilder 文本 = new StringBuilder();
    Files.lines(file.toPath()).forEach(new Consumer<String>() {

      @Override
      public void accept(String t) {
        文本.append(t + "\n");
      }

    });
    return 文本.toString();
  }

  public static void writeLinesToFile(Collection<String> lines, String fileName) {
    try {
      BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
      for (String line : lines) {
        out.write(line + "\n");
      }
      out.close();
    } catch (IOException e) {
    }
  }

  public static void mkdir(String path) {
    File outputDir = new File(path);
    if (!outputDir.exists() && !outputDir.isDirectory()) {
      outputDir.mkdir();
    }
  }
}
