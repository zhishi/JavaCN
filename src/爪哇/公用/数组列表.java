
package 爪哇.公用;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class 数组列表<元素类> {

  private ArrayList<元素类> 对象;

  public 数组列表() {
    对象 = new ArrayList<>();
  }

  public 数组列表(int 初始容量) {
    对象 = new ArrayList<>(初始容量);
  }

  public 数组列表(Collection<? extends 元素类> 集) {
    对象 = new ArrayList<>(集);
  }

  public boolean 添加(元素类 元素) {
    return 对象.add(元素);
  }

  public void 添加(int 索引, 元素类 元素) {
    对象.add(索引, 元素);
  }

  public boolean 添加所有(int 索引, Collection<? extends 元素类> 集) {
    return 对象.addAll(索引, 集);
  }

  public boolean 添加所有(Collection<? extends 元素类> 集) {
    return 对象.addAll(集);
  }

  public void 清空() {
    对象.clear();
  }

  public Object 克隆() {
    return 对象.clone();
  }

  public boolean 包含(Object 甲) {
    return 对象.contains(甲);
  }

  public void 确保容量(int arg0) {
    对象.ensureCapacity(arg0);
  }

  public void 对每个(Consumer<? super 元素类> 行为) {
    对象.forEach(行为);
  }

  public 元素类 取(int 索引) {
    return 对象.get(索引);
  }

  public 元素类 置(int 索引, 元素类 元素) {
    return 对象.set(索引, 元素);
  }

  public int 取索引(Object 甲) {
    return 对象.indexOf(甲);
  }

  public int 取末索引(Object 甲) {
    return 对象.lastIndexOf(甲);
  }

  public Object[] 转数组() {
    return 对象.toArray();
  }

  public <类型> 类型[] 转数组(类型[] 组) {
    return 对象.toArray(组);
  }

  public 元素类 删除(int 索引) {
    return 对象.remove(索引);
  }

  public boolean 删除(Object 甲) {
    return 对象.remove(甲);
  }

  public boolean 删除所有(Collection<?> 集) {
    return 对象.removeAll(集);
  }

  public boolean 保留所有(Collection<?> 集) {
    return 对象.retainAll(集);
  }

  public ListIterator<元素类> 列表迭代器(int 索引) {
    return 对象.listIterator(索引);
  }

  public ListIterator<元素类> 列表迭代器() {
    return 对象.listIterator();
  }

  public Iterator<元素类> 迭代器() {
    return 对象.iterator();
  }

  public List<元素类> 子列表(int 起索引, int 止索引) {
    return 对象.subList(起索引, 止索引);
  }

  public Spliterator<元素类> 分割迭代器() {
    return 对象.spliterator();
  }

  public boolean 条件删除(Predicate<? super 元素类> 过滤器) {
    return 对象.removeIf(过滤器);
  }

  public void 替换所有(UnaryOperator<元素类> 操作符) {
    对象.replaceAll(操作符);
  }

  public void 排序(Comparator<? super 元素类> 比较器) {
    对象.sort(比较器);
  }

  public void 缩减大小() {
    对象.trimToSize();
  }

  public int 大小() {
    return 对象.size();
  }
}
